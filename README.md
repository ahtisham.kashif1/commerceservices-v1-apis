# CommerceServices-V1 APIS

This project contains commerce APIs which is related to Identity Management.
Guidelines for runnin

We are automating the commerce service v1 APIs by using Rest Assured

What is Rest Assured?

REST Assured is a Java library for testing RESTful web services It is used to invoke REST web services and check response, Can be used to test XML as well as JSON based web services It supports POST, GET, PUT, DELETE, OPTIONS, PATCH, and HEAD requests and can be used to validate and verify the response of these requests

## Getting started
List of some tools and libraries used:

1. TestNG
2. Java
3. Maven 
3. Rest Assured 
Prerequists:
We should be having 
   - JAVA - After installation, you need to set Java & its bin folder path on the below platforms.
   For Mac -  On bash profile
   For windows -  On envirnment variable
   - IDE (Eclipse/Intellij) 
   - Maven - https://maven.apache.org/download.cgi
   For Maven you need to download its Binary zip archive version & you need to set the Maven_home & bin folder path on the below    platforms, if you are using any of them.
   For Mac -  on Bash Profile
   For windows - on envirnment variable 
## Tools and Framework information
- TestNG
   TestNG -Test automation framework developed using Java and Selenium with page object model. Developed as a Maven project that can be easily integrated with Jenkins and run the test.

- Maven 
   Maven is a popular open-source build tool developed by the Apache Group to build, publish, and deploy several projects at once for better project management. The tool provides allows developers to build and document the lifecycle framework. 

Installation of Maven 
   a. Install Maven on Windows - https://www.baeldung.com/install-maven-on-windows-linux-mac#installing-maven-on-windows
   b. Install Maven on Linux - https://www.baeldung.com/install-maven-on-windows-linux-mac#installing-maven-on-linux
   c. Install Maven on Mac OSX- https://www.baeldung.com/install-maven-on-windows-linux-mac#installing-maven-on-mac-os-x

## About the Project
Test automation framework contains following packages and files as shown in the below image.
Project Name: CommerceService
Packages details: 
1. commerce_Service
2. v1_scenario

commerce_service: This package contains all the classes for new user registration, user login, Get user details, Reset/Update user password, Create/Update/delete address. All the static methods that are defined in the each page class can be re used in test classes by importing the class to perform actions on different apis.

2. v1_scenario: This package containts the classes which contains many other scenarios like negative test cases of v1 apis.These testcases  validate the login after updation of password with old/new password, try to access user details by using other user id.

3.	Xml files.
   a.	pom.xml - Contains all the plugins and dependencies of testng/json-sample that are required to run the test as maven project. 
   b. reports.xml - Contains all the details of the classes of the commerce_service, contains the all class names that are to be triggered to run complete suiote of the test cases as this is testng xml file & the purpose of this xml file to run multiple classes to 1 xml file.
   c. Scenarioreport.xml -  contains all the details of the classes of v1_scenario package & purpose of this xml file is to run multiple classes of the scenario package through 1 xml file.




## How to execute the tests
1. First need to download the Java and set the java and bin path to mentioned areas.
2. Download the Maven depencides and set the maven home and bin path to mentioned areas
3. Clone the project for gitlab and import the project into the any IDE.
4. Import the downloaded project from the path and it will show the POM File, finish the complete step by clicking finish button.


